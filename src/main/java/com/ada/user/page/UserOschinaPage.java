package com.ada.user.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.user.entity.UserOschina;
public class UserOschinaPage extends PageRpc<UserOschina> {

	public UserOschinaPage(Pagination<UserOschina> page) {
		super(page);
	}

}
