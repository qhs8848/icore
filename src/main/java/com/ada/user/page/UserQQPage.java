package com.ada.user.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.user.entity.UserQQ;
public class UserQQPage extends PageRpc<UserQQ> {

	public UserQQPage(Pagination<UserQQ> page) {
		super(page);
	}

}
