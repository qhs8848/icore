package com.ada.user.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.user.entity.UserWeiXin;
public class UserWeiXinPage extends PageRpc<UserWeiXin> {

	public UserWeiXinPage(Pagination<UserWeiXin> page) {
		super(page);
	}

}
