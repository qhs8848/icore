package com.ada.user.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.user.entity.UserOauthWeibo;
public class UserOauthWeiboPage extends PageRpc<UserOauthWeibo> {

	public UserOauthWeiboPage(Pagination<UserOauthWeibo> page) {
		super(page);
	}

}
