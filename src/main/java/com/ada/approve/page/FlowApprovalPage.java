package com.ada.approve.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.approve.entity.FlowApproval;
public class FlowApprovalPage extends PageRpc<FlowApproval> {

	public FlowApprovalPage(Pagination<FlowApproval> page) {
		super(page);
	}

}
