package com.ada.user.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.user.entity.UserFriend;

public class UserFriendPage extends PageRpc<UserFriend>{

	public UserFriendPage(Pagination page) {
		super(page);
	}

}
