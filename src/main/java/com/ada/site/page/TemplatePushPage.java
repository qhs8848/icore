package com.ada.site.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.site.entity.TemplatePush;
public class TemplatePushPage extends PageRpc<TemplatePush> {

	public TemplatePushPage(Pagination<TemplatePush> page) {
		super(page);
	}

}
