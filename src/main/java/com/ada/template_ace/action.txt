package #{action_p};



import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ada.data.page.Order;
import com.ada.data.page.Page;
import com.ada.data.page.Pageable;
import #{entity_p}.#{Entity};
import #{manager_p}.#{Entity}Service;
@Controller
public class #{Entity}Action {
	private static final Logger log = LoggerFactory.getLogger(#{Entity}Action.class);

	@RequestMapping("/#{config_entity}/view_list")
	public String list(Pageable pageable, HttpServletRequest request, ModelMap model) {
	
		if (pageable==null) {
			pageable=new Pageable();
		}
		if (pageable.getOrders()==null||pageable.getOrders().size()==0) {
			pageable.getOrders().add(Order.desc("id"));
		}
		Page<#{Entity}> pagination = manager.findPage(pageable);
		model.addAttribute("list", pagination.getContent());
		model.addAttribute("page", pagination);
		return "#{config_entity}/list";
	}

	@RequestMapping("/#{config_entity}/view_add")
	public String add(ModelMap model) {
		return "#{config_entity}/add";
	}

	@RequestMapping("/#{config_entity}/view_edit")
	public String edit(Pageable pageable,#{idtype} id, Integer pageNo, HttpServletRequest request, ModelMap model) {
		model.addAttribute("model", manager.findById(id));
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("page", pageable);
		return "#{config_entity}/edit";
	}

	@RequestMapping("/#{config_entity}/model_save")
	public String save(#{Entity} bean, HttpServletRequest request, ModelMap model) {
	
	    String view="redirect:view_list.htm";
		try {
			bean = manager.save(bean);
			log.info("save object id={}", bean.getId());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("erro", e.getMessage());
			view="#{config_entity}/add";
		}
		return view;
	}

	@RequestMapping("/#{config_entity}/model_update")
	public String update(Pageable pageable, #{Entity} bean,HttpServletRequest request, ModelMap model) {
		
		String view="redirect:/#{config_entity}/view_list.htm?pageNumber="+pageable.getPageNumber();
		try {
		bean = manager.update(bean);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("erro", e.getMessage());
			model.addAttribute("model",bean);
		    model.addAttribute("page", pageable);
			view="#{config_entity}/edit";
		}
		return view;
	}

	@RequestMapping("/#{config_entity}/model_delete")
	public String delete(Pageable pageable, #{idtype} id, HttpServletRequest request, ModelMap model) {
			 
				manager.deleteById(id);
			 
		return "redirect:/#{config_entity}/view_list.htm?pageNumber="+pageable.getPageNumber();
	}
	@RequestMapping("/#{config_entity}/model_deletes")
	public String deletes(Pageable pageable, #{idtype}[] ids, HttpServletRequest request, ModelMap model) {
			 
				manager.deleteByIds(ids);
			 
		return "redirect:/#{config_entity}/view_list.htm?pageNumber="+pageable.getPageNumber();
	}
	@Autowired
	private #{Entity}Service manager;
}