package com.ada.site.page;

import com.ada.data.core.Pagination;
import com.ada.data.dto.PageRpc;
import com.ada.site.entity.TemplateNotice;
public class TemplateNoticePage extends PageRpc<TemplateNotice> {

	public TemplateNoticePage(Pagination<TemplateNotice> page) {
		super(page);
	}

}
